package test;
import model.*;
import view.View;

import static org.junit.Assert.*;
import org.junit.Test;

import controller.Controller;

public class Test1 {
	

	

	@Test
	public void testAdd() {
		Polinom p1 ,p2 ,result;
		p1 = Polinom.polinomize("+2*x^4");
		p2 = Polinom.polinomize("-5*x^5");
		result = p1.add(p2);
		
		assertTrue(result.toString().equals("+(-5.0*X^5) +(2.0*X^4) "));
	}
	
	
	
	@Test
	public void testSub() {
		Polinom p1 ,p2 ,result;
		p1 = Polinom.polinomize("+2*x^4");
		p2 = Polinom.polinomize("-5*x^5");
		result = p1.sub(p2);
		
		assertTrue(result.toString().equals("+(5.0*X^5) +(2.0*X^4) "));
	}
	
	@Test
	public void testMul() {
		Polinom p1 ,p2 ,result;
		p1 = Polinom.polinomize("+2*x^4");
		p2 = Polinom.polinomize("-5*x^5");
		result = p1.mul(p2);
		
		assertTrue(result.toString().equals("+(-10.0*X^9) "));
	}
	
	@Test
	public void testDiv1() {
		Polinom p1 ,p2 ;
		Polinom[] result;
		p1 = Polinom.polinomize("+2*x^4");
		p2 = Polinom.polinomize("-5*x^5");
		result = p1.div(p2);
		
		assertTrue(result[0].toString().equals("0"));
	}
	
	@Test
	public void testDiv2() {
		Polinom p1 ,p2 ;
		Polinom[] result;
		p2 = Polinom.polinomize("+2*x^4");
		p1 = Polinom.polinomize("-5*x^5");
		result = p1.div(p2);
		
		assertTrue(result[0].toString().equals("+(-2.5*X^1) "));
	}
	
	@Test
	public void testDiv3() {
		Polinom p1 ,p2 ;
		Polinom[] result;
		p1 = Polinom.polinomize("0");
		p2 = Polinom.polinomize("-5*x^5");
		result = p1.div(p2);
		
		assertTrue(result[0].toString().equals("0"));
	}
	
	@Test
	public void testDiv4() {
		Polinom p1 ,p2 ;
		Polinom[] result;
		p1 = Polinom.polinomize("+1*X^3-1*X^2");
		p2 = Polinom.polinomize("+1*X^2-1*X^0");
		result = p1.div(p2);
		
		assertTrue(result[0].toString().equals("+(1.0*X^1) +(-1.0*X^0) ") && result[1].toString().equals("+(1.0*X^1) +(-1.0*X^0) ")  );
	}
	
	@Test
	public void testDeriv() {
		Polinom p1 ,result;
		p1 = Polinom.polinomize("+2*x^4");
		result = p1.deriv();

		assertTrue(result.toString().equals("+(8.0*X^3) "));
	}
	
	public void testInteg() {
		Polinom p1 ,result;
		p1 = Polinom.polinomize("+2*x^4");
		result = p1.integ();

		assertTrue(result.toString().equals("+(0.4*X^5) "));
	}
	
	@Test
	public void testView() {
		View view = new View();
		
		String s1="-5*x^5";
		String s2="+2*x^4";
		view.setCalcSolution(Polinom.polinomize(s1).add(Polinom.polinomize(s2)));

		Controller contr = new Controller(view);
		
		assertTrue( view.getCalcSolution().equals("+(-5.0*X^5) +(2.0*X^4) "));
	}
	
}
